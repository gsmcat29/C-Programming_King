#include <stdio.h>

int main()
{
    printf("Wacky Linux (c) 2023 Wacky Software, Inc\n");
    printf("Compiled on %s at %s\n", __DATE__, __TIME__);

    return 0;
}