/*
The Q&A section at the end of this chapter show how the strcmp function might be
written using array subscripting. Modify the function to use pointer arithmetic
instead 
*/

#include <stdio.h>
#include <string.h>

int strcmp_KR(char *s, char *t);
int strcmp_KR_ptr(char *s, char *t);

int main()
{
    char str1[] = "perro";
    char str2[] = "perro";
    int answer;

    //answer = strcmp_KR(str1, str2);
    answer = strcmp_KR_ptr(str1, str2);

    printf("The value of compare is %d\n", answer);

    return 0;
}

int strcmp_KR(char *s, char *t)
{
    int i;

    for (i = 0; s[i] == t[i]; i++) {
        if (s[i] == '\0')
            return 0;
    }

    return s[i] - t[i];
}

int strcmp_KR_ptr(char *s, char *t)
{
    //int i;
    char *p = s;
    char *q = t;

    for (;*p == *q; p++, q++) {
        if (*p == '\0')
            return 0;
    }

    return *p - *q;
}


