/*
Suppose that p has been declared as follows:

char *p = "abc";

Which of the following function calls are legal? Show the output produced by
each legal call, and explain why the other are illegal
*/

#include <stdio.h>


int main()
{
    char *p = "abc";

    //putchar(p); Illegal, waiting for single character
    putchar(*p);
    puts("");
    puts(p);
    //puts(*p); Illegal, causes segmentation fault waiting for string not pointer

    printf("\n");
    return 0;
}