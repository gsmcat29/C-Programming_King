/* The following function supposedly creates an identical copy of a string.
What's wrong with the function? */

#include <stdio.h>
#include <string.h>

char *duplicate(const char *p);

int main()
{
    char s1[80] = "gato";
    char *s2;


    s2 = duplicate(s1);

    puts(s1);

    return 0;
}

char *duplicate(const char *p)
{
    char *q = p;

    strcpy(q, p);

    return q;
}

/*
answer: without initialization we generation a segmentation fault
*/

