/*
Use techniques of Section 13.6 to condense the count_spaces function of Section
13.4. In particular, replace the for statement by a while loop
*/
#include <stdio.h>

int count_spaces_ptr(const char *s);
int count_spaces(const char s[]);

int main()
{
    char str[] = "Example with multiple spaces .";
    //puts(str);
    int spaces = 0;
    spaces = count_spaces_ptr(str);
    printf("Number of spaces pointer: %d\n", spaces);

    spaces = count_spaces(str);
    printf("Number of spaces subscript: %d\n", spaces);
    

    return 0;
}

int count_spaces_ptr(const char *s)
{
    int count = 0;

    /*for (; *s != '\0'; s++) {
        if (*s == ' ')
            count++;
    }*/
    while (*s != '\0') {
        if (*s == ' ')
            count++;
        s++;
    }

    return count;
}

int count_spaces(const char s[])
{
    int count = 0, i;

    for (i = 0; s[i] != '\0'; i++) {
        if (s[i] == ' ')
            count++;
    }

    return count;
}