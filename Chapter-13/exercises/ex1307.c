/*
Suppose that str is an array of characters. Which one of the following 
statements is not equivalent to the other three
*/

#include <stdio.h>
#include <string.h>

int main()
{
    char str[] = "gato";

    //*str = 0;
    //str[0] = '\0';
    //strcpy(str, "");
    strcat(str, "");

    puts(str);

    return 0;
}

/**
 * Letter d), because we are concatenating two strings
*/