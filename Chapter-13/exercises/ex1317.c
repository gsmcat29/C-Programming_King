/*
write the following function:

bool test_extension(const char *file_name, const char *extension);

file_name point to a string containing a file name. The function should return
true if the file's extension matches the string pointed to by extension, ignoring
the case of letters.

For example, the call test_extension("memo.txt", "TXT") would return true.
Incorporate the "search for the end of a string" idiom in your function. 
Hint: Use the toupper function to conver characcter to upper-case before comparing
them
*/

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h> 

bool test_extension(const char *file_name, const char *extension);

int main()
{
    bool result;

    result = test_extension("memo.txt", "TXT");

    if (result) {
        printf("true\n");
    }
    else {
        printf("false\n");
    }

    return 0;
}

bool test_extension(const char *file_name, const char *extension)
{
    size_t n;
    char suffix[10];
    char upper[10];

    for (n = 0; *file_name != '\0'; file_name++) {
        if (file_name[n] == '.') {
            printf("Contains file extension\n");
            ++n;
            suffix[0] = file_name[n];
            ++n;
            suffix[1] = file_name[n];
            ++n;
            suffix[2] = file_name[n];

            break;
        }
        
        n++;
    }

    //puts(suffix);

    for (size_t i = 0; i < strlen(suffix); ++i) {
        upper[i] = toupper(suffix[i]);
    }

    //puts(upper);

    if ((strcmp(upper,extension)) == 0)
        return true;
    else
        return false;
}