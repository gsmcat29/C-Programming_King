/*
What does the following program print?
*/

#include <stdio.h>

int main(void)
{
    char s[] = "Hsjodi", *p;

    for (p = s; *p; p++)
        --*p;

    puts(s);

    return 0;
}

/*
answer:

It prints the previous letter in the string:

H s j o d i
G r i n c h
*/