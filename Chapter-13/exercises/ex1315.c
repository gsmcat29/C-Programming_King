/*
Let f be the following function:

a) What is the value of f("abcd", "babc")?
b) What is the value of f("abcd", "bcd") ?
c) In general what value does f return when passed two strings s and t?

*/

/*
answers
a) 3
b) 0
c) Return the number of consecutive letter from firs string
*/

#include <stdio.h>
#include <string.h>

int f(char *s, char *t);

int main()
{
  int answer;
 char s1[] = "abcd";
 char t1[] = "babc";

 char s2[] = "abcd";
 char t2[] = "bcd";

  answer = f(s1, t1);
  printf("Answer: %d\n", answer);

  answer = f(s2, t2);
  printf("Answer: %d\n", answer);

  return 0;
}

int f(char *s, char *t)
{
    char *p1, *p2;

    for (p1 = s; *p1; p1++) {
        for (p2 = t; *p2; p2++) {
            if (*p1 == *p2) break;
        }
        if (*p2 == '\0') break;
    }

    return p1 - s;
}