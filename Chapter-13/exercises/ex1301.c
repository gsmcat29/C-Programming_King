/*
The following function calls supposedly write a single new-line character, but
some are incorrect. Identify which calls don't work and why.

*/

#include <stdio.h>

int main()
{
    printf("Random text\n");;
    //printf("\n");
    //putchar("\n");
    puts("");
    printf("More random text\n");

    return 0;
}



/*

a) printf("%c", '\n'); -- prints new line
b) printf("%c", "\n"); -- INCORRECT. We are expecting a character not string
c) printf("%s", '\n'); -- INCORRECT. Segmentation fault, expects string
d) printf("%s", "\n"); -- prints new line
e) printf('\n');       -- INCORRECT. Segmentation fault, expects string
f) printf("\n");       -- prints new line
g) putchar('\n');      -- prints new line
h) putchar("\n");      -- INCORRECT. Waiting for character
i) puts('\n);          -- INCORRECT. Segmentation fault, waiting string
j) puts("\n");         -- prints double new line
k) puts("");           -- prints single new line
*/