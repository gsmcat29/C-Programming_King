/*
a) Write a function named capitalize that capitalizes all letters in its 
argument. The argument will be a null-terminated string containing arbitrary
characters, not just letters
Use array subscripting to access the characters in the string.

Hint: Use toupper function to convert each character to upper-case

b) Rewrite capitalize function, this time using pointer arithmetic to access the
characters  in the string
*/

#include <stdio.h>
#include <ctype.h>


void capitalize(char str[]);
void capitalize_ptr(char *str);

int main()
{
    char str[] = "per^r+o";
    char str2[] = "ga^t+o";

    capitalize(str);
    puts(str);

    capitalize_ptr(str2);
    puts(str2);

    return 0;
}

void capitalize(char str[])
{
    int i = 0;

    while(str[i] != '\0') {
        str[i] = toupper(str[i]);
        i++;
    }
}

void capitalize_ptr(char *str)
{
    while(*str) {
        *str = toupper(*str);
        str++;
    }
}