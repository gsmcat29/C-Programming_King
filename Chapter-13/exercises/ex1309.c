/*
What will be the value of the string s1 after the following statements have
been executed?

strcpy(s1, "computer");
strcpy(s2, "science");
if (strcmp(s1, s2) < 0)
    strcat(s1, s2);
else
    strcat(s2, s1);

s1[strlen(s1-6)] = '\0'
*/

#include <stdio.h>
#include <string.h>

int main()
{
    char s1[80] = "";
    char s2[80] = "";

    strcpy(s1, "computer");
    strcpy(s2, "science");
    if (strcmp(s1, s2) < 0)
        strcat(s1, s2);
    else
        strcat(s2, s1);

    // before this line str1 = computerscience
    s1[strlen(s1)-6] = '\0';

    // prints: computers
    puts(s1);

    return 0;
}