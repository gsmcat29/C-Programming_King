/*
Write the following function

void get_extension(const char *file_name, char *extension);

file_name  point to a string containing a file name. The function should store
the extension on the file name in the string pointed to by the extension.
For example, if the file name is "memo.txt", the function will store "txt" in 
the string pointed to by  the extension. If the file name doesn't have an
extension, the function should store an empty string (a single null character)
in the string pointed to by extension. Keep the functionn as simple as possible
by having it use the strlen and strcpy functions
*/
#include <stdio.h>
#include <string.h>

void get_extension(const char *file_name, char *extension);

int main()
{
    char fileName[] = "memo";
    char ext[10];

    get_extension(fileName, ext);

    return 0;
}


void get_extension(const char *file_name, char *extension)
{
    char suffix[10];

    for (int i = 0; i < strlen(file_name); ++i) {
        if (file_name[i] == '.') {
            printf("Contains file extension\n");
            ++i;
            suffix[0] = file_name[i];
            ++i;
            suffix[1] = file_name[i];
            ++i;
            suffix[2] = file_name[i];

            break;
        }

    }

    //printf("Suffix = ");
    //puts(suffix);

    strcpy(extension, suffix);
    puts(extension);
}