/*
Write the following function:

void remove_filename(char *url);

url points to a string containing a URL (Uniform  Resource Locator) that ends
with a file name (such as "http://www.knking.com/index.html").
The function should modify the string by removing the file name and the preceding
slash. (In this example, the result will be "http://www.knking,com").
Incorporate the "search for the end of a string" idiom into your function.
Hint: Have the function replace the last slash in the string by a null character
*/

#include <stdio.h>
#include <string.h> 

void remove_filename(char *url);

int main()
{
    char test[80] = "http://www.knking.com/index.html";
    remove_filename(test);
    puts(test);

    return 0;
}

void remove_filename(char *url)
{
    size_t n = 0;

    // transver string one pass
    while(*url)
        url++;

    // transverse string second pass, but reverse order to find first(last) /
    while(*url-1) {
        url--;
        if (*url == '/') {
            *url = '\0';
            break;
        }
    }

}

