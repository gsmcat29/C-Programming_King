/*
Suppose that we call scanf as follows:

scanf("%d%s%d", &i, s, &j);

If the user enters 12abc34 56def78, what will be the values of i, s, and j after
the call? (Assume that i,j are int variables and s is an array of characters)
*/

#include <stdio.h>

int main()
{
    int i = 0;
    int j = 0;
    char s[80];

    scanf("%d%s%d", &i, s, &j);

    printf("i: %d\n", i);
    printf("j: %d\n", j);
    printf("s: ");
    puts(s);
    
    return 0;
}

/*
For 12abc34
i = 12
j = 0
s = abc34

For 56def78
i = 56
j = 0
s = def78

For 12abc34 56def78

i = 12:
j = 56
s = abc34
*/