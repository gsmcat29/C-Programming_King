/* What will be the value of string str after the following statements have
been executed?

strcpy(str, "tire-bouchon");
strcpy(&str[4], "d-or-win");
strcat(str, "red");

*/

#include <stdio.h>
#include <string.h>

int main()
{
    char str[80] = "";

    //printf("Before using statements: ");
    //puts(str);

    printf("After statements:\n");
    strcpy(str, "tire-bouchon\n");
    strcpy(&str[4], "d-or-win");
    strcat(str, "red?");
    puts(str);

    return 0;
}

/**
 * answer: tired-or-winred?
*/