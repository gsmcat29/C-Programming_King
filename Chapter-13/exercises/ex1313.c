/*
Write the following function:

void build_index_url(const char *domain, char *index_url);

domain points to a string containing an Internet domain, such as "knking.com"
The function should add "http://www." to the beginning of this string and 
"/index.html" to the end of the stirng, storing the result in the string pointed
to by index_url. 
(In this example, the result will be "http://www.knking.com/index.html")
You may assume  that index_url point ot a variable that is long enough to hold
the resulting string. Keep the functioin as simple as possible by having it use
the strcat and strcpy functions
*/

#include <stdio.h>
#include <string.h>

void build_index_url(const char *domain, char *index_url);

int main()
{
    char domain[] = "knking.com";
    char index[80];

    build_index_url(domain, index);

    return 0;
}

void build_index_url(const char *domain, char *index_url)
{
    char prefix[80] = "https://www.";
    char suffix[80] = "/index.html";

    strcat(prefix, domain);
    strcat(prefix, suffix);
    strcpy(index_url,prefix);

    puts(index_url);
}