// strlen idiom
#include <stdio.h>
#include <string.h>

size_t my_strlen(const char *s);

int main()
{
    char str_test[] = "catpuccino";

    size_t n0 = strlen(str_test);
    size_t n1 = my_strlen(str_test);

    printf("Using library strlen: %ld\n", n0);
    printf("Using my own strlen: %ld\n", n1);

    return 0;
}

size_t my_strlen(const char *s)
{
    size_t n = 0;

    for (; *s != '\0'; s++) {
        n++;
    }

    return n;
}