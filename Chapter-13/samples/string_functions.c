// using the C string library
#include <stdio.h>
#include <string.h>

#define N 80

int main()
{
    // strcpy function *********************************************************
    //char *str1; // If you want this type of initialization, malloc is required
    //char *str2;
    char str1[N];
    char str2[N];

    printf("strcpy function **********************************************\n");

    strcpy(str2, "abcd");
    strcpy(str1, str2);

    printf("str2: ");
    puts(str2);
    printf("str1: ");
    puts(str1);

    // strlen function *********************************************************
    printf("strlen function ***********************************************\n");
    int len = 0;
    len = strlen("abc");        // len is now 3
    printf("len: %d\n", len);
    len = strlen("");
    printf("len: %d\n", len);
    len = strlen(str1);
    printf("len: %d\n", len);
    // strcat function *********************************************************
    printf("strcat function ***********************************************\n");

    char str3[N];
    char str4[N];

    strcpy(str3, "abc");
    strcat(str3, "def");    // str3 now contains "abcdef"
    puts(str3);

    return 0;
}