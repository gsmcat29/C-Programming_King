// writing strings using printf and puts
#include <stdio.h>

int main()
{
    char str[] = "Are we having fun yet?";

    printf("%s\n", str);

    // to print just a part of the string
    printf("%.6s\n", str);

    // using puts
    puts(str);

    return 0;
}