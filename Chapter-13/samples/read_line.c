// own version to read a string
#include <stdio.h>

#define N 80

int read_line(char str[], int n);

int main()
{
    char str[N];

    printf("%d ", read_line(str, N));

    printf("\n");

    return 0;
}

int read_line(char str[], int n)
{
    int ch, i = 0;

    while ((ch = getchar()) != '\n')
        if (i < n) {
            str[i++] = ch;
            printf("%c ", ch);  // to print character
        }
    str[i] = '\0';              // terminates a string

    return i;
} 