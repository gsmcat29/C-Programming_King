#include <stdio.h>

char digit_to_hex_char(int digit)
{
    return "0123456789ABCDEF"[digit];
}

int main()
{
    char *p;
    p = "abc";

    printf("p = %c\n", *p); // prints a
    ++p;
    printf("p = %c\n", *p); // prints b

    char ch;
    ch = "abc"[1];
    printf("ch = %c\n", ch);
    
    printf("Enter a value from 0 - 15 to convert: ");
    int value = 0;

    scanf("%d", &value);

    char convert = digit_to_hex_char(value);

    printf("convert = %c\n", convert);


    return 0;
}
