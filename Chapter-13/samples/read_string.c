// read strings using scanf and gets
#include <stdio.h>

#define SENT_LEN 80

int main()
{
    char sentence0[SENT_LEN+1];
    char sentence1[SENT_LEN+1];

    printf("Enter a sentence:\n");

    printf("Begins scanf:\n");
    scanf("%s", sentence0);

    // now using gets
    printf("Begins gets:\n");
    gets(sentence1);

    // print values
    puts(sentence0);
    puts(sentence1);

    return 0;
}