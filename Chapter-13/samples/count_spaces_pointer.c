// pointer version to count spaces in a string
#include <stdio.h>

int count_spaces(const char *s);

int main()
{
    char str[] = "Example with multiple spaces .";
    //puts(str);
    int spaces = 0;
    spaces = count_spaces(str);

    printf("Number of spaces: %d\n", spaces);
    

    return 0;
}

int count_spaces(const char *s)
{
    int count = 0;

    for (; *s != '\0'; s++) {
        if (*s == ' ')
            count++;
    }

    return count;
}