// own version of strcat
#include <stdio.h>
#include <string.h>

char *my_strcat(char *s1, const char *s2);

int main()
{
    char str1[] = "perro";
    char str2[] = "gato";

    char str3[] = "perro";
    char str4[] = "gato";    
    
    strcat(str1, str2);

    printf("Using library strcat:\n");
    puts(str1);


    my_strcat(str3, str4);
    printf("Using my own strcat:\n");
    puts(str3);

    return 0;    
}

char *my_strcat(char *s1, const char *s2)
{
    char *p = s1;

    while(*p) {// while(*p != '\0')
        p++;
    }

    while (*s2 != '\0') {
        *p = *s2;
        p++;
        s2++;
    }

    *p = '\0';

    return s1;
}


