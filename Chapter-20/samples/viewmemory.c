// Allows the user to view regions of computer memory
#include <ctype.h>
#include <stdio.h>
#include <stdint.h>

typedef unsigned char BYTE;

int main(void)
{
  uint32_t addr;
  int32_t i, n;
  BYTE *ptr;

  printf("Address of main function: %x\n", (uint32_t ) main);
  printf("Address of addr variable: %x\n", (uint32_t ) &addr);

  printf("\nEnter a (hex) address: ");
  scanf("%x", &addr);
  printf("Enter number of bytes to view: ");
  scanf("%d", &n);

  printf("\n");
  printf("Address              Bytes                             Characters\n");
  printf("-------   -------------------------------------       -----------\n");

  ptr = (BYTE *) addr;
  for (; n > 0; n-= 10) {
    printf("%8X   ", (uint32_t) ptr);
    for (i = 0; i < 10 && i < n; i++)
      printf("%.2X ", *(ptr + i));
    for (; i < 10; i++)
      printf("  ");
    printf("  ");
    for (i = 0; i < 10 && i < n; i++) {
      BYTE ch = *(ptr + i);
      if (!isprint(ch))
        ch = '.';
      printf("%c", ch);
    }
    printf("\n");
    ptr += 10;
  }

  return 0;
}