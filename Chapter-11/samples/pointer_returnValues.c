#include <stdio.h>

#define N 6

// Returns a pointer to whichever integer is larger
int *max(int *a, int *b);

// Returns a pointer to the middle element of the array a
int *find_middle(int a[], int n);

int main()
{
    int *p;
    int *q;
    int i = 25;
    int j = 50;

    int a[N] = {1,2,3,4,5,6};

    p = max(&i, &j);
    q = find_middle(a, N);

    printf("The larger value is: %d\n", *p);
    printf("The middle element is %d\n", *q);
}

int *max(int *a, int *b)
{
    if (*a > *b)
        return a;
    else
        return b;
}

int *find_middle(int a[], int n)
{
    return &a[n/2];
}