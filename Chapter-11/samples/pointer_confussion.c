#include <stdio.h>

int main()
{
    int i, j, *p, *q;
    p = &i;
    q = &j;
    i = 1;

    // *q will receive the value that *p points to, 1
    *q = *p;
    printf("*p = %d\n", *p);
    printf("*q = %d\n", *q);

    return 0;
}