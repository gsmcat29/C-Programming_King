#include <stdio.h>

int main()
{
    int i, j, *p, *q;
    p = &i;     //the address of i is copied into p
    q = p;      // copies content of p (address of i) into q

    *p = 1;
    printf("*p = %d\n", *p);
    printf("*q = %d\n", *q);

    *q = 2;
    printf("*p = %d\n", *p);
    printf("*q = %d\n", *q);

    return 0;
}