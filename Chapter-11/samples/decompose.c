#include <stdio.h>

void decompose(double x, long *int_part, double *frac_part);

int main()
{
    double x = 3.14159;
    long i = 0;
    double d = 0;

    decompose(x, &i, &d);

    printf("int_part = %ld\n", i);
    printf("fac_part = %lf\n", d);

    return 0;
}

void decompose(double x, long *int_part, double *frac_part)
{
    *int_part = (long) x;
    *frac_part = x - *int_part;
}