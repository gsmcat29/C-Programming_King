#include <stdio.h>

int main()
{
    int i;
    int *p; // points nowhere in particular

    p = &i;
    i = 1;

    printf("i = %d\n", i);
    printf("*p = %d\n", *p);

    *p = 2;

    printf("i = %d\n", i);
    printf("*p = %d\n", *p);

    return 0;
}