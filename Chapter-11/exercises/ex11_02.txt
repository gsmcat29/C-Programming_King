If i is an int variable and p and q are pointers to int, which of the
following assigments are legal?

(a) p = i;      (d) p = &q;     (g) p = *q;
(b) *p = &i;    (e) p = *&q;    (h) *p = q;
(c) &p = q;     (f) p = q;      (i) *p = *q


Legal: (e), (f), (i)