/*
Write a loop that prints all temperature readings stored in row i of the 
temperatures array (see exercise 14). Use a pointer to visit each element of the
row
*/

#include <stdio.h>
#include <stdbool.h>

bool search(const int a[], int n, int key);

int main()
{
    int temperature[7][24] = {{32,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4},
                              {1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4},
                              {1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4},
                              {1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4},
                              {1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4},
                              {1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4},
                              {1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4},
                            };

    bool answer;

    printf("Did I found the value? ");
    answer = search(temperature, 7 * 24, 32);
    printf("%s\n", answer ? "true" : "false");

    // print array
    int *p, i = 0;
    for (p = temperature[i]; p < temperature[i] + 24; ++p) {
        printf("%d ", *p);
    }
   

    printf("\n");

    return 0;
}

// POINTER ARITHMETIC VERSION ==================================================
bool search(const int *a, int n, int key)
{
    //bool ans;
    int *p;

    for (p = &a[0]; p < &a[n]; ++p) {
        if (*a == key) {
            return true;
        }
    }

    return false;
}
