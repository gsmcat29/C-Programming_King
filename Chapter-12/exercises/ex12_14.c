/*
Assume that the following array contains a week's worth of hourly temperature
readings, with each row containing the readings for one day:

int temperature[7][24];

write a statement that uses the search function (see Exercise 7) to search the
entire temperatures array for the value 32
*/

#include <stdio.h>
#include <stdbool.h>

bool search(const int a[], int n, int key);

int main()
{
    int temperature[7][24] = {{32,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4},
                              {1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4},
                              {1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4},
                              {1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4},
                              {1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4},
                              {1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4},
                              {1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4},
                            };

    bool answer;

    printf("Did I found the value? ");
    answer = search(temperature, 7 * 24, 32);
    printf("%s\n", answer ? "true" : "false");

    return 0;
}

// POINTER ARITHMETIC VERSION ==================================================
bool search(const int *a, int n, int key)
{
    //bool ans;
    int *p;

    for (p = &a[0]; p < &a[n]; ++p) {
        if (*a == key) {
            return true;
        }
    }

    return false;
}
