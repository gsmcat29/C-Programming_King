/*
Write the evaluate_position function descrived in Exercise 13 of Chapter 9. Use
pointer arithmetic--not subscripting--to visit array elements. Use a single loop
instead of nested loops
*/

#include <stdio.h>

int evaluate_position(char board[8][8]);

int main()
{
    // test 
    char b_test[8][8];

    int ans = evaluate_position_ptr(b_test);
    printf("Test = %d\n", ans);

    return 0;
}

// FUNCTION DEFINITION =========================================================
int evaluate_position(char board[8][8])
{
    int whitePieces = 0; int blackPieces = 0;

    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            switch(board[i][j])
            {
                case 'Q': whitePieces += 9;     break;
                case 'R': whitePieces += 5;     break;
                case 'B': whitePieces += 3;     break;
                case 'N': whitePieces += 3;     break;
                case 'P': whitePieces += 1;     break;
                case 'q': blackPieces += 9;     break;
                case 'r': blackPieces += 5;     break;
                case 'b': blackPieces += 3;     break;
                case 'n': blackPieces += 3;     break;
                case 'p': blackPieces += 1;     break;
            }
        }
    }

    return (whitePieces - blackPieces);
}

int evaluate_position_ptr(char board[8][8])
{
    int whitePieces = 0; int blackPieces = 0;
    char *p;
    int i = 0;

    for (p = board[i]; p < board + 64; ++p) {

        switch(*p)
        {
            case 'Q': whitePieces += 9;     break;
            case 'R': whitePieces += 5;     break;
            case 'B': whitePieces += 3;     break;
            case 'N': whitePieces += 3;     break;
            case 'P': whitePieces += 1;     break;
            case 'q': blackPieces += 9;     break;
            case 'r': blackPieces += 5;     break;
            case 'b': blackPieces += 3;     break;
            case 'n': blackPieces += 3;     break;
            case 'p': blackPieces += 1;     break;
        }        
    }

    return (whitePieces - blackPieces);
}