/*
Modify the find_middle function of Section 11.5 so that it uses pointer 
arithmetic to calculate return value
*/

#include <stdio.h>
#define N 5

int *find_middle(int a[], int n);
int *find_middle_ptr(int *a, int n);

int main()
{
    int test[] = {1,2,3,4,5};

    int *out = find_middle_ptr(test, N);

    printf("Middle element is: %d\n", *out);

    return 0;
}

int *find_middle(int a[], int n)
{
    return &a[n/2];
}

int *find_middle_ptr(int *a, int n)
{
    //return &a[n/2];
    return a + n/2;
}