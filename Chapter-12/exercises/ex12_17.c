/*
Rewrite the following function to use pointer arithmetic instead of array 
subscripting. (In other words, eliminate the variables i and j and all uses of 
the [] operator.) Use a single loop instead of nested loops.
*/

#include <stdio.h>
#define LEN 2

int sum_two_dimensional_array(const int a[][LEN], int n);
int sum_two_dimensional_array_ptr(const int a[][LEN], int n);   

int main()
{
    int arr_a[][LEN] = {{1,2}, {3,4}};
    int n = 2;
    int result = 0;

    result = sum_two_dimensional_array_ptr(arr_a, n);

    printf("Result: %d\n", result);

    return 0;
}

// ARRAY VERSION ===============================================================
int sum_two_dimensional_array(const int  a[][LEN], int n)
{
    int i, j, sum = 0;

    for (i = 0; i < n; i++)
        for (j = 0; j < LEN; j++)
            sum += a[i][j];
    
    return sum;
}

// POINTER ARITHMETIC VERSION ==================================================
int sum_two_dimensional_array_ptr(const int a[][LEN], int n)
{
    int *p;
    int i = 0;
    int sum = 0;
    // p = a[i] == p = &a[i][0]

    for (p = a[i]; p < a[i] + n * LEN; ++p) {
        sum += *p;
    }

    return sum;
}