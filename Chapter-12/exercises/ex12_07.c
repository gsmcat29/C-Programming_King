/*
Write the following function:

bool search(const int a[], int n, int key)

a is an array to be searched, n is the number of elements in the array, and key
is the search key. search should return true if key matches some element of a, 
and false if it doesn't. Use pointer arithmetic --not subcripting-- to visit
array elements
*/

#include <stdio.h>
#include <stdbool.h>

bool search(const int a[], int n, int key);

int main()
{
    int key_value = 0;
    int n_elements = 0;
    int number = 0;
    bool answer;

    printf("Please enter number of element in the array:  ");
    scanf("%d", &n_elements);

    int a[n_elements];

    printf("Please enter the value of that array:\n");

    for (int i = 0; i < n_elements; ++i) {
        scanf("%d", &number);
        a[i] = number;
    }

    printf("Please enter a key value: ");
    scanf("%d", &key_value);


    answer = search(a, n_elements, key_value);
    printf("Did I find that value? ");
    printf("%s", answer ? "true" : "false");

    // print array element to test =============================================
    /*printf("The values stored in array are:\n");

    for (int j = 0; j < n_elements; ++j) {
        printf("%d\t", a[j]);
    }*/
    // =========================================================================
    
    printf("\n");

    return 0;
}

// SUBSCRIPTING VERSION ========================================================
/*bool search(const int a[], int n, int key)
{
    //bool ans;

    for (int i = 0; i < n; ++i) {
        //scanf("%d", &number);
        //a[i] = number;
        if (a[i] == key) {
            return true;
        }
    }

    return false;
}*/

// POINTER ARITHMETIC VERSION ==================================================
bool search(const int *a, int n, int key)
{
    //bool ans;
    int *p;

    for (p = &a[0]; p < &a[n]; ++p) {
        if (*a == key) {
            return true;
        }
    }

    return false;
}


