/*
Section 8.2 had a program fragment in which two nested for loops initialized the 
array ident for use as an identity matrix. Rewrite this code, using a single 
pointer to step through the array one element at a time. Hint: Since we won't be 
using row and col index variables, it won't be easy to tell where to store 1. 
Instead, we can use the fact that the first element of the array should be 1, 
the next N elements should be 0, the next element should be 1 and so forth. 
Use a variable to kep track of how many consecutive 0s have been stored; when 
the count reaches N, it's time to store 1.
*/

#include <stdio.h>

#define N 5

void ident_matrix(int n, double ident[n][n]);
void ident_matrix_ptr(int n, double ident[n][n]);

int main()
{
    double ident[N][N];
    int n = N;
    // init matrix
    ident_matrix_ptr(n, *ident);


    // print ident matrix
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            printf("%.2f\t", ident[i][j]);
        }
        printf("\n");
    }  

}

void ident_matrix(int n, double iden[n][n])
{
    int row, col;

    for (row = 0; row < N; row++) {
        for (col = 0; col < N; col++) {
            if (row == col)
                iden[row][col] = 1.0;
            else
                iden[row][col] = 0.0;
        }
    } 
    
     
}

void ident_matrix_ptr(int n, double iden[n][n])
{
    double *p;
    int n_zeros = 0;


    for(p = &iden[0][0]; p <= &iden[n-1][n-1]; ++p) {

        if (n_zeros <= N && n_zeros != 0) {
            *p = 0;
        }
        else {
            *p = 1;
            n_zeros = 0;
        }
        ++n_zeros;
    }
    
     
}