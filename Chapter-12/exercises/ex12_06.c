/*
Rewrite the following function to use pointer arithmetic instead of array 
subscripting (In other words, eliminate the variable i and all uses of [] 
operator). Make as few changes as possible
*/

#include <stdio.h>
#define N 5

int sum_array(const int a[], int n);
int sum_array_ptr(const int a[], int n);

int main()
{
    int a[N] = {1,2,3,4,5};
    int sum = sum_array(a, N);
    int sum_ptr = sum_array(a,N);

    printf("total sum is: %d\n", sum);
    printf("total sum ptr is: %d\n", sum_ptr);

    return 0;
}

// ============================================================================
int sum_array_ptr(const int a[], int n)
{
    int *p;
    int sum;
    sum = 0;

    for (p = &a[0]; p < &a[n]; p++)
        sum += *p;
    
    return sum;
}

int sum_array(const int a[], int n) // int sum_array(const int *a, int n)
{
	int i, sum;

	sum = 0;
	for (i = 0; i < n;  i++)
		sum += a[i];

	return sum;
}