/*
Rewrite the make_empty, is_empty, and is_full functions of Section 10.2 to use
the pointer variable top_ptr instead of the integer variable top
*/

#include <stdbool.h>	// C99 only
#include <stdio.h>

#define STACK_SIZE	100
// Test to remove warning
void stack_overflow();

// External Variables
int contents[STACK_SIZE];

int *top_ptr = NULL;

void make_empty(void)
{

    top_ptr = &contents[0];
}

bool is_empty(void)
{
	return top_ptr == &contents[0]
}

bool is_full(void)
{
	return top_ptr == &contents[0];
}

void push(int i)
{
	if (is_full())
		stack_overflow();
	else
		contents[top++] = i;
}

int pop(void)
{
    if (is_empty())
		stack_underflow();
	else
		return contents[--top];
}