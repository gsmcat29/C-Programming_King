/*
Write the following function:

double inner_product(const double *a, const double *b, int n);

a and b both are point to arrays of length n. The function should return 
a[0] * b[0] + a[1] * b[1] + ... + a[n-1] * b[n-1].

Use pointer arithmetic to visit array elements

*/

#include <stdio.h>

double inner_product(const double *a, const double *b, int n);

int main()
{
    int n = 3;
    double a[] = {1, 2, 3};
    double b[] = {2, 4, 6};
    double output = 0;

    printf("Computing ..\n");

    output = inner_product(a, b, n);

    printf("The result of inner product is: %.2lf\n", output);
    
    return 0;
}

double inner_product(const double *a, const double *b, int n)
{
    double ans = 0;


    for (int i = 0; i < n; ++i) {
        // using post-increment pointer
        ans += *(a++) * *(b++);
    }

    return ans;
}