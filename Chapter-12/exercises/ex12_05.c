/*
Suppose that a is a one-dimensional array and p is a pointer variable.
Assuming that the assignment p = a has just been performed, which of the
following expressions are illegal because of mismatched types? Of the remaining
expressions, which are true (have a nonzero value)?

a) p == a[0]            // Illegal, trying to compare int and pointer
b) p == &a[0]           // Legal
c) *p == a[0]           // Legal
d) p[0] == a[0]         // Legal
*/

#include <stdio.h>

int main()
{
    int a[] = {1,2,3,4,5};
    int *p;

    p = a;

    printf("p == &a[0] : %d\n", p == a[0]);
    printf("p == &a[0] : %d\n", p == &a[0]);
    printf("p == &a[0] : %d\n", *p == a[0]);
    printf("p == &a[0] : %d\n", p[0] == a[0]);


    return 0;
}