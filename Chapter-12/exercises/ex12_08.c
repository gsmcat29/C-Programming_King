/*
Rewrite the following function to use pointer arithmetic instead of array
subscripting (In other words, eliminate the variable i and all uses of [] 
operator) Make as few changes as possible
*/

#include <stdio.h>

void store_zeros(int a[], int n);

int main()
{
    int n_elements = 0;

    printf("How many elements? ");
    scanf("%d", &n_elements);

    int a_arr[n_elements];

    store_zeros(a_arr, n_elements);

    printf("The values stored in array are:\n");
    for (int j = 0; j < n_elements; ++j) {
        printf("%d\t", a_arr[j]);
    }

    printf("\n");

    return 0;
}

// SUBSCRIPTING VERSION ========================================================
/*void store_zeros(int a[], int n)
{
    int i;

    for (i = 0; i < n; i++)
        a[i]= 0;
}*/

// POINTER ARITHMETIC VERSION ==================================================
void store_zeros(int *a, int n)
{
    int *p;

    for (p = &a[0]; p < &a[n]; ++p)
        *p = 0;
}