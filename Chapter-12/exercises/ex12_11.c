/*
Modify the find_largest function so that is uses pointer arithmetic --not 
subscripting-- to visit array element
*/

#include <stdio.h>
#define N 5

int find_largest(int a[], int n);
int find_largest_ptr(int *a, int n);

int main()
{
    int arr_a[] = {10, 20, 30, 50, 40};
    int largest = 0;

    largest = find_largest_ptr(arr_a, N);

    printf("The largest value is: %d\n", largest);

    return 0;
}

int find_largest(int a[], int n)
{
    int i, max;
    max = a[0];
    for (i = 1; i < n; i++) {
        if (a[i] > max)
            max = a[i];
    }

    return max;
}

int find_largest_ptr(int *a, int n)
{
    int max = 0;
    int *p = &a[0];

    for (p = a; p < a + n; ++p)
    {
        if (*p > max)
            max = *p;
    }

    return max;
}