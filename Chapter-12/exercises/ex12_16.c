/*
Write a loop that prints the highest temperature in the temperatures array (see 
Exercise 14) for each day of the week. The loop body should call the 
find_largest function, passing it one row of the array at a time.
*/

#include <stdio.h>
#include <stdbool.h>

bool search(const int a[], int n, int key);
int find_largest_ptr(int *a, int n);

int main()
{
    int temperature[7][24] = {{32,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4},
                              {11,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4},
                              {1,22,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4},
                              {1,2,33,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4},
                              {1,2,3,44,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4},
                              {1,2,3,4,55,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4},
                              {1,2,3,4,5,66,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4},
                            };

    bool answer;

    printf("Did I found the value? ");
    answer = search(temperature, 7 * 24, 32);
    printf("%s\n", answer ? "true" : "false");

    for (int i = 0; i < 7; ++i) {
        printf("Max Temp: %d\n", find_largest_ptr(temperature[i], 24));
    }

    return 0;
}

// POINTER ARITHMETIC VERSION ==================================================
bool search(const int *a, int n, int key)
{
    //bool ans;
    int *p;

    for (p = &a[0]; p < &a[n]; ++p) {
        if (*a == key) {
            return true;
        }
    }

    return false;
}

// =============================================================================
int find_largest_ptr(int *a, int n)
{
    int max = 0;
    int *p = &a[0];

    for (p = a; p < a + n; ++p)
    {
        if (*p > max)
            max = *p;
    }

    return max;
}