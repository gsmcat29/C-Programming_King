/*
What will be  the contents of the array after the following statements are
executed?

#define N 10

int a[N] = {1,2,3,4,5,6,7,8,9,10};
int *p = &a[0], *q = &a[N-1], temp;

while (p < q) {
    temp = *p;
    *p++ = *q;
    *q-- = temp;
}

*/

#include <stdio.h>

#define N 10

int main()
{
    int a[N] = {1,2,3,4,5,6,7,8,9,10};
    int *p = &a[0], *q = &a[N-1], temp;

    while (p < q) {
        temp = *p;
        *p++ = *q;
        *q-- = temp;
    }

    for (int i = 0; i < N; ++i) {
        printf("%d\t", a[i]);
    }

    printf("\n");

    return 0;    
}

// ans: It reverses the array, 10   9    8    7    6    5    4    3     2     1