/*
Write the following function:

void find_two_largest(const int *a, int n, int *largest, int *second_largest);

a points to an array of length n. The function searches the array for its 
largest and second largest elements, storing them in the variables pointed to by
largest and second_largest, respectively. Use pointer arithmetic--not 
subscripting--to visit array elements
*/
#include <stdio.h>
#define N 5

void find_two_largest(const int *a, int n, int *largest, int *second_largest);

int main()
{
    int arr_a[] = {10, 20, 30, 40, 50};

    int largest_value = 0;
    int second_largest_value = 0;

    int *largest_ptr;
    largest_ptr = &largest_value;
    int *second_ptr;
    second_ptr = &second_largest_value;


    find_two_largest(arr_a, N, largest_ptr, second_ptr);



    return 0;
}

void find_two_largest(const int *a, int n, int *largest, int *second_largest)
{
    int value1 = 0;
    int value2 = 0;

    int *p;     // largest
    int *q;     // second largest


    p = &a[0];
    q = &a[1];

    if (*p > *q) {
        value1 = *p;
        value2 = *q;
    }
    else {
        value1 = *q;
        value2 = *p;
    }

    for (p = &a[2]; p < a + n; ++p) {
        if (*p > value1) {
            value2 = value1;
            value1 = *p;
        }
    }

    printf("Largest value: %d\n", value1);
    printf("Second largest value: %d\n", value2);
}
