/*
(a) Write a program that reads a messafe, then checks wheter it's a palindrome
(the letters in the message are the same from left to right and viceversa)

Enter a message = He llived as a devil, eh?
Palindrome

Enter a message = Madam, I am Adam
Not palindrome
*/

// https://github.com/williamgherman/c-solutions/blob/master/12/projects/02/2.c

#include <stdio.h>
#include <ctype.h>

int main()
{
    char message[80];
    char ch;
    char *p;
    char *q;
    int len = 0;

    p = message;

    printf("Enter a message:: ");
    while((ch = tolower(getchar())) != '\n' && p < message + 80) {
        if (isalpha(ch)){
            *p = ch;
            ++p;
            ++len;
        }
    }

    --p;

    for (q = message; q < p; q++, --p) {
        if (*p != *q) {
            printf("Not a palindrome\n");
            return 0;
        }
    }

    printf("Palindrome\n");    

    return 0;
}