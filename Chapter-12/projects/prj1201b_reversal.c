/*

Write a program that reads a message, then prints the reversal of the message:

Enter a message: Don't get, mad get even.
Reversal is: .neve teg, dam teg t'onD

Hint: Read the message one character at a time (using getchar) and store the
characters in an array. Stop reading when the array is full or character read is
'\n'

(B)  Revise the program to use a pointer instead of an integer to keep track of
current position in the array
*/


#include <stdio.h>
#include <stdlib.h>

int main()
{
    char message[80];       // for reversal
    int ch;
    char *p;
    //p = 

    printf("Enter a message: ");
    ch = getchar();
    int idx = 0;
    int len = 0;

    while (ch != EOF && (idx < 80)) {
        message[idx] = ch;
        ++idx;
        ++len;
        ch = getchar();
    }

    p = &message[0];
    while (ch != EOF && (idx < 80)) {
        *p = ch;
        ++len;
        ++idx;
        ch = getchar();
    }

    //printf("len: %d\n", len);
    printf("\nReversal is:\t\b");
    //printf("%s\n", message); -> normal message

    // reversal message
    for(p = &message[0] + len-1; p >= &message[0]; --p) {
        //printf("%c", *p);
        putchar(*p);
    }


    printf("\n");

    return 0;
}