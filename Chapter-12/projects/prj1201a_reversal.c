/*
Write a program that reads a message, then prints the reversal of the message:

Enter a message: Don't get, mad get even.
Reversal is: .neve teg, dam teg t'onD

Hint: Read the message one character at a time (using getchar) and store the
characters in an array. Stop reading when the array is full or character read is
'\n'
*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
    char message[80];       // for reversal
    int ch;

    printf("Enter a message: ");
    ch = getchar();
    int idx = 0;
    int len = 0;

    while (ch != EOF && (idx < 80)) {
        message[idx] = ch;
        ++idx;
        ++len;
        ch = getchar();
    }

    printf("len: %d\n", len);
    printf("Reversal is: ");
    //printf("%s\n", message); -> normal message

    // reversal message
    for (int i = len; i >= 0; --i) {
        printf("%c", message[i]);
    }

    printf("\n");

    return 0;
}