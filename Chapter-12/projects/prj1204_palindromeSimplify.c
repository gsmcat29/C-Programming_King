/*
Simplify Programming Project 2 by taking advantage of the fact that an array
name can be used as a pointer
*/

#include <stdio.h>
#include <ctype.h>

int main()
{
    char message[80];
    char ch;
    char *p;
    char *q;
    int len = 0;

    p = message;

    printf("Enter a message:: ");
    while((ch = tolower(getchar())) != '\n' && p < message + 80) {
        if (isalpha(ch)){
            *p = ch;
            ++p;
            ++len;
        }
    }

    --p;

    for (q = message; q < p; q++, --p) {
        if (*p != *q) {
            printf("Not a palindrome\n");
            return 0;
        }
    }

    printf("Palindrome\n");    

    return 0;
}