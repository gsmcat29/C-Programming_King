// subtracting integer to pointer
#include <stdio.h>

int main()
{
    int a[10] = {0,1,2,3,4,5,6,7,8,9};
    int *p, *q;

    p = &a[8];
    printf("p = %d\n", *p);

    q = p - 3;
    printf("q = %d\n", *q);

    p -= 6;
    printf("p = %d\n", *p);

    return 0;
}