// using a for loop with pointer arithmetic
#include <stdio.h>
#define N 10

int main()
{
    int sum = 0, *p;

    int a[N] = {11,34,82,7,64,98,47,18,79,20};

    for (p = &a[0]; p < &a[N]; p++) {
        sum += *p;
    }

    printf("Sum = %d\n", sum);

    return 0;
}