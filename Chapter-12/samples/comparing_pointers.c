// compare two pointer from same array
#include <stdio.h>

int main()
{
    int a[10], *p, *q, i = 0;

    p = &a[5];
    q = &a[1];

    // print addresses
    printf("&p = %p\n", p);
    printf("&q = %p\n", q);

    // print values of comparison
    printf("p <= q ? = %d\n", p <= q);
    printf("p >= q ? = %d\n", p >= q);


    return 0;
}