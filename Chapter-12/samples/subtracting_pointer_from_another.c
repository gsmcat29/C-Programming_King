// subracting one pointer from another
#include <stdio.h>

int main()
{
    int a[10], *p, *q, i = 0;

    p = &a[5];
    q = &a[1];

    // print addresses
    printf("&p = %p\n", p);
    printf("&q = %p\n", q);

    // print values of i
    i = p - q;
    printf("i = %d\n", i);    
    i = q - p;
    printf("i = %d\n", i);

    printf("&p = %p\n", p);
    printf("&q = %p\n", q);

    return 0;
}